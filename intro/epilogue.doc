\chapter*{Epilogue For This Book}
\label{chap:epiloqueThisBook}

\section{Version 0.4.2}
It was surprising to receive
over 14,000 downloads and 227 thank eMail (non
from U.S.A.) and some other reactions.
More surprising is the fact this
textbook has sections that are cutting edge research\footnote{
An individual asked this author to examine a paper on Triple
Shock Entropy Theorem and Its Consequences. 
Overall, the paper has some good points.
Yet, it seems that the authors were not kept today knowledge.
For example, they dedicated their entire Appendix pp. 26-31 to find
the maximum point on oblique shock (see equation
\eqref{oblique:eq:D} of this book).
This author hopes that after more resaschers familiarize themselves with
the analytical solution, the research papers will be simplified.}. 

The focus of this version was mostly on the oblique shock and related
issues as results of questions and reactions on this topic.
However, most people reached to www.potto.org by 
searching for either terms ``Rayleigh flow'' (107) and ``Fanno
flow'' ((93).
If the total combined variation search of terms
``Fanno'' and ``Rayleigh'' (mostly through google)
is accounted, it reaches to about 30\% (2011). 
This fact must be considered.
Thus, most the next version will be concentrated on
the missing parts/section in Fanno flow and Rayleigh flow.

Furthermore, the questions that appears on the net will guide this
author on what is really need to be in compressible flow book. 
At this time, several questions were about compressibility factor
in Fanno flow and other kind of flow models.
The other question that appear related in several chambers
connecting to each other.
Also, a individual asked this author to start to write about
the unsteady section, and it will be down the line.

\section{Version 0.4}
Since the last version (0.3) several individuals sent me remarks and
suggestions.
In the introductory chapter, extensive description of the compressible
flow history was written.
The chapter on speed of sound added the two phase aspects.
The isothermal nozzle was added to the chapter. 
Some example were added to the normal shock chapter.
The fifth chapter deals now with normal shock in variable area ducts.
The sixth chapter deals with external forces fields.
The chapter about oblique shock was added and it contains
the analytical solution.
At this stage, the connection between Prandtl--Meyer flow
and oblique is an note form. 
The a brief chapter on Prandtl--Meyer flow was added.



\section{Version 0.3}
In the traditional class of compressible flow it is assumed that
the students will be aerospace engineers or dealing mostly
with construction of airplanes and turbomachinery.
This premise should not be assumed.
This assumption drives students from other fields away from this 
knowledge.
This knowledge should be spread to other fields
because it needed there as well.
This ``rejection'' is especially true when students feel 
that they have to go  through a ``shock wave'' in their understanding.

This book is the second book in the series of POTTO project books.
POTTO project books are open content textbooks.
The reason the topic of Compressible Flow 
was chosen, while relatively
simple topics like fundamentals of strength of material were delayed,
is because of the realization that manufacture engineering simply
lacks fundamental knowledge in this area and thus produces faulty 
designs and understanding of major processes.
Furthermore, the undersigned observed that many researchers who are
dealing with
manufacturing processes are clueless about fluid mechanics in general
but particularly in relationship to compressible flow.
In fact one of the reasons that many manufacturing jobs are
moving to other countries is because of the lack of understanding of
fluid mechanics in general and compressible in particular.
For example, the lack of competitive advantage moves many of the die casting
operations to off shore\footnote{Please read the undersigned's book
``Fundamentals of Die Casting Design,'' which
demonstrates how ridiculous design and research can be.}.
It is clear that an understanding of Compressible Flow is 
very important for areas that traditionally have ignored the
knowledge of this topic\footnote{%
The fundamental misunderstanding of choking results in poor 
models (research) in the area of die casting, which in turn
results in many bankrupt companies and the movement of the die casting
industry to offshore.}.

As many instructors can recall from their time as undergraduates,
there were classes during which most students had a period of
confusion, and then later, when the dust settled, almost
suddenly things became clear.
This situation is typical also for Compressible Flow classes,
especially for external compressible flow (e.g. flow around a wing, etc.).
This book offers a more balanced emphasis which
focuses more on internal compressible flow than the traditional classes.
The internal flow topics seem to be common for the ``traditional''
students  and students from other fields, e.g., manufacturing 
engineering.

This book is written in the spirit of my adviser and mentor E.R.G.~Eckert.
Who, aside from his research activity, wrote the book that brought a 
revolution in the heat transfer field of education.
Up to Eckert's book, the study of heat transfer was without 
any dimensional analysis.
He wrote his book because he
realized that the dimensional analysis utilized by him and his adviser,
Nusselts, and their colleagues, must be taught in engineering classes.
His book met strong criticism in which some called to burn his book.
Today, however, there is no known place in world that does not
teach according to Eckert's doctrine.
It is assumed that the same kind of individuals who criticized 
Eckert's work will criticize this work.
This criticism will not change the future or the success of the ideas in 
this work.
As a wise person says ``don't tell me that it is wrong,
show me what is wrong''; this is the only reply.
With all the above, it must be emphasized that this book will
not revolutionize the field even though considerable
new materials that have never been published are included.
Instead, it will provide a new emphasis and new angle to Gas Dynamics.

Compressible flow is essentially different from incompressible flow in
mainly two respects: discontinuity (shock wave) and choked 
flow.
The other issues, while important, are not that crucial 
to the understanding
of the unique phenomena of compressible flow.
These unique issues of compressible flow are to be emphasized
and shown.
Their applicability to real world processes is to be
demonstrated\footnote{If you have better and different examples
or presentations you are welcome to submit them.}.

The book is organized into several chapters which, as a traditional
textbook, deals with a basic introduction of thermodynamics concepts
(under construction).
The second chapter deals with speed of sound. 
The third chapter provides the first example of choked
flow (isentropic flow in a variable area).
The fourth chapter  deals with a simple case
of discontinuity (a simple shock wave in a nozzle).
The next chapter is dealing with isothermal flow with and without
external forces (the moving of the choking point), again
under construction.
The next three chapters are dealing with three models of
choked flow: Isothermal flow\footnote{It is suggested to referred
to this model as Shapiro flow}, Fanno flow and Rayleigh flow.
First, the Isothermal flow is introduced because of the relative ease
of the analytical treatment.
Isothermal flow  provides useful tools for the pipe systems design.
These chapters are presented almost independently.
Every chapter can be ``ripped'' out and printed independently.
The topics of filling and evacuating of gaseous chambers are presented,
normally missed from traditional textbooks.
There are two advanced topics which included here: oblique shock wave, and
properties change effects (ideal gases and real gases) (under
construction).
In the oblique shock, for the first time analytical solution is 
presented, 
which is excellent tool to explain the strong, weak and
unrealistic shocks.
The chapter on one-dimensional unsteady state, is currently under
construction.

The last chapter deals with the computer program, 
Gas Dynamics Calculator.
The program design and how to use the program are described
(briefly).

Discussions on the flow around bodies (wing, etc),
and Prandtl--Meyer expansion
will be included only after the gamma version unless
someone will provide discussion(s) (a skeleton) on these topics.

It is hoped that this book will serve the purposes that was
envisioned for the book.
It is further hoped that others will contribute to this book
and find additional use for this book and enclosed software.



